package com.TimeReport.Back.repositories;

import com.TimeReport.Back.models.ReportModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReportRepository extends MongoRepository<ReportModel, String> {

    public List<ReportModel> findByCoduser(String coduser);
}
