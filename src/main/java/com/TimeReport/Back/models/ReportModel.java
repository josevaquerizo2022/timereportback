package com.TimeReport.Back.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reports")
public class ReportModel {

    @Id
    //@JsonProperty(access= JsonProperty.Access.READ_ONLY)
    private String id;
    //private Optional<String id>;
    private String coduser;
    private String nombre;
    private String tarea;
    private int horas;


    //se hace un constructor sin parametros
    public ReportModel() {
    }
    //se hace otro constructor con todos los parametros
    public ReportModel(String id, String coduser, String nombre, String tarea, int horas) {
        this.id = id;
        this.coduser = coduser;
        this.nombre = nombre;
        this.tarea = tarea;
        this.horas = horas;
    }

    //además incluimo stodos los getter y setter
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTarea() {
        return this.tarea;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    public float getHoras() {
        return this.horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public String getCoduser() {return this.coduser;}

    public void setCoduser(String coduser) {this.coduser = coduser;}

    public String getNombre() {return this.nombre;}

    public void setNombre(String nombre) {this.nombre = nombre;}
}