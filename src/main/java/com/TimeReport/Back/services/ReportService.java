package com.TimeReport.Back.services;


        import com.TimeReport.Back.models.ReportModel;
        import com.TimeReport.Back.repositories.ReportRepository;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.data.domain.Sort;


        import org.springframework.stereotype.Service;


        import java.util.List;
        import java.util.Optional;

@Service
public class ReportService  {

    @Autowired
    ReportRepository reportRepository;
//LISTAR TODO

    public List<ReportModel> getReports(String orderBy){
        System.out.println("getreport en reportService");

        List<ReportModel> result;

        if (orderBy != null){
            System.out.println("Se ha pedido ordenación");
            result = this.reportRepository.findAll(Sort.by("horas"));
        } else {
            result = this.reportRepository.findAll();
        }

        return result;
    }

    public List<ReportModel> findByCoduser(String coduser){
        System.out.println("Optional en ReportService");

        return this.reportRepository.findByCoduser(coduser);
    }

    public Optional<ReportModel> findById(String id){
        System.out.println("Optional en ReportService");

        return this.reportRepository.findById(id);
    }

    public ReportModel add(ReportModel report){
        System.out.println("add en ReportService");
/*
        List<ReportModel> result;

        result = this.reportRepository.findAll(Sort.by(Sort.Direction.DESC,"id"));
        Integer idmayor = Integer.parseInt( result.get(0).getId());
        idmayor += 1;
        report.setId(idmayor.toString());
*/
        report.setId(null);
        return this.reportRepository.insert(report);
    }

    public ReportModel update(ReportModel reportModel) {
        System.out.println("update en ReportModel");

        return this.reportRepository.save(reportModel);
    }

    public boolean delete(String id) {

        System.out.println("delete en PS");
        boolean result = false;
        if (this.findById(id).isPresent() == true){
            System.out.println("Reporte a borrar encontrado");
            this.reportRepository.deleteById(id);
            result = true;
        }

        return result;
        //findById al ser un optional hay que validarlo
        //this.productRepository.findById(id); no se usa porque NO lleva implicita "las normas de negocio"


    }
}