package com.TimeReport.Back.controllers;

import com.TimeReport.Back.models.ReportModel;
import com.TimeReport.Back.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
//Este req mapping puesto aquí es como una ruta general para todas las conexiones
@RequestMapping("/reports")
//IMPORTANTE PARA EL HACKATON
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
//El resultado de que no esté correcto, si no ponemos esto habrá problemas con el CORS
public class ReportController {


    @Autowired
    ReportService reportService;

//LISTAR TODO

    @GetMapping("/reports")
    public ResponseEntity<List<ReportModel>> getReports(
            @RequestParam (name = "$orderby", required = false) String orderBy
    ){
        System.out.println("getReports");

        return new ResponseEntity<>(
                this.reportService.getReports(orderBy),
                HttpStatus.OK
        );
    }


    @GetMapping("/reports/{coduser}")
    public ResponseEntity<List<ReportModel>> getReportById(@PathVariable String coduser) {
        System.out.println("getReportById");
        System.out.println("La id del usuario a buscar es = " + coduser);

        List<ReportModel> result = this.reportService.findByCoduser(coduser);
        System.out.println("ini return  new ResponseEntity" + result);

        return  new ResponseEntity<>(
                result,
                //result.isPresent() ? result.get(): "Reporte no econtrado",
                result.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK

        );


        //(Condición) = vale_essto_si_true : vale_esto_si_false
    }

    @PostMapping("/reports")
    public ResponseEntity<ReportModel> addReport(@RequestBody ReportModel report){
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + report.getId());
        System.out.println("El código de usuario a crear es " + report.getCoduser());
        System.out.println("El nombre del usuario a crear es " + report.getNombre());
        System.out.println("La tarea a crear es " + report.getTarea());
        System.out.println("Las horas del usuario a crear es " + report.getHoras());

        return new ResponseEntity<>(
                this.reportService.add(report),
                HttpStatus.CREATED
        );
    }
    @PutMapping("/reports/{id}")
    public  ResponseEntity<ReportModel> updateReport(
            @RequestBody ReportModel reportModel, @PathVariable String id
    ){
        System.out.println("updateReport");
        System.out.println("La id del reporte a actualizar en parametro URL es" + id);
        System.out.println("La id del reporte a actualizar es" + reportModel.getId());
        System.out.println("El código de usuario del reporte a actualizar es " + reportModel.getCoduser());
        System.out.println("El nombre del usuario del reporte a actualizar es " + reportModel.getNombre());
        System.out.println("La tarea del reporte es" + reportModel.getTarea());
        System.out.println("Las horas del reporte a actualizar  es" + reportModel.getHoras());

        Optional<ReportModel> reportToUpdate = this.reportService.findById(id);

        if (reportToUpdate.isPresent()){
            System.out.println("Tarea encontrado, actualizando");
            this.reportService.update(reportModel);

        }

        return  new ResponseEntity<>(
                reportModel,
                reportToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/reports/{id}")
    public ResponseEntity<String> deleteId(@PathVariable String id){
        System.out.println("deleteReporte");
        System.out.println("La id del reporte a borrar es: " + id);

        boolean deleteId = this.reportService.delete(id);

        return new ResponseEntity<>(
                deleteId ? "Reporte borrada" : "Reporte no borrada",
                deleteId ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
