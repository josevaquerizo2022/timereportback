package com.TimeReport.Back.controllers;


import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class HelloController {

    @RequestMapping("/")
    public String index(){
        return "Hola Mundo desde TimeReportBAck!";
    }

    @RequestMapping("/hello")
    public String hello(
            @RequestParam(value = "name", defaultValue = "TimeReport") String name)
    {
        return String.format("Hola %s!", name);
    }
}

